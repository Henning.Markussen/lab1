package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        // TODO: Implement Rock Paper Scissors
        while(true) {
            //Human
            if (roundCounter!=1 && readInput("Do you wish to continue playing? (y/n)?").toLowerCase().equals("n")) break;
            System.out.println("Let's play round "+roundCounter);
            String userPlay = readInput("Your choice (Rock/Paper/Scissors)?");
            while (!rpsChoices.contains(userPlay.toLowerCase())){
                System.out.println( "I do not understand "+ userPlay+". Could you try again?");
                userPlay = readInput("Your choice (Rock/Paper/Scissors)?");
            }

            //Robot
            int robotPlayIndex = new Random().nextInt(rpsChoices.size());
            String robotPlay = rpsChoices.get(robotPlayIndex);

            String winner_string = "Human chose "+userPlay+", computer chose "+robotPlay+".";

            if (decideWinner(userPlay, robotPlay)){
                System.out.println(winner_string+" Human wins!");
                humanScore+=1;
            }
            else if(decideWinner(robotPlay, userPlay)){
                System.out.println(winner_string+" Computer wins!");
                computerScore+=1;
            }
            else{
                System.out.println(winner_string+" It's a tie!");
            }
            System.out.println("Score: human "+humanScore+", computer "+computerScore);
            roundCounter+=1;
        }
        System.out.println("Bye bye :)");
    }
    public boolean decideWinner(String opt1, String opt2){
        if (opt1.equals("paper")) {
            return opt2.equals("rock");
        }
        else if (opt1.equals("scissors")){
            return opt2.equals("paper");
        }
        else{
            return opt2.equals("scissors");
        }
    }
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
